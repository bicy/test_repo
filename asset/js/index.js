$(document).ready(function(){

  $("#productType").change(function(){

     var val = $(this).find('option:selected').val();
    
     switch(val){
        case '1': 
            $("#Book").removeClass("hidden");
            $('#DVD, #Furniture').addClass('hidden');
            break;
        case '2':
          $("#DVD").removeClass('hidden');
            $("#Book, #Furniture").addClass('hidden');
            break;
        case '3':
            $('#Furniture').removeClass('hidden');
            $("#DVD, #Book").addClass('hidden');
            break;
        default:
            $('#Book, #DVD, #Furniture').addClass('hidden');
     }
  });

});