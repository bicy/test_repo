$(document).ready(function(){

   $("#product_form").validate({

   	 errorElement:'small',

		rules:{
			sku:{
				required:true,
				minlength:4,
			},
			name:{
				required:true,
				minlength:3,
				maxlength:50
			},
			price:{
				required:true,
				digits:true
			},
			pr_category:{
				selectcheck:true,
			},
			weight:{
				required:true,
				digits:true
			},
			size:{
				required:true,
				digits:true
			},
			height:{
				required:true,
				digits:true
			},
			width:{
				required:true,
				digits:true
			},
			length:{
				required:true,
				digits:true
			}
			

		},
		messages:{

			sku:{
				required:'Please, submit required data',
				minlength:jQuery.validator.format("At least {0} characteres required"),
			},
			name:{
				required:'Please, submit required data',
				minlength:jQuery.validator.format("At least {0} characteres required"),
			},
			price:{
				required:'Please submit required data',
				digits:'Enter a valid price greater than 0',
			},
			weight:{
				required:'Please, submit required data',
				digits:'Enter a valid weight',
			},
			size:{
				required:'Please, submit required data',
				digits:'Enter a valid size'
			},
			height:{
				required:'Please, submit required data',
				digits:'Enter a valid height'
			},
			width:{
				required:'Please, submit required data',
				digits:'enter a valid width'
			},
			length:{
				required:'Please, submit required data',
				digits:'enter a valide width',
			}
		}
   });

	  jQuery.validator.addMethod('selectcheck', function (value) {
	    return (value != '0');
	}, "Switcher is required");

})

