<?php

class DbManager{

   private $servername;
	private $username;
	private $password;
	private $dbname;
	private $con;

    public function __construct($servername, $username, $password, $dbname){
    	$this->servername = $servername;
    	$this->username = $username;
    	$this->password = $password;
    	$this->dbname = $dbname;

    }
	public function add($product, $category_id){

       $this->con = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
		// Check connection
		if ($this->con->connect_error) {
		  die("connection error :".$this->con->connect_error);
		}

        $name = $product->getName();
        $sku = $product->getSku();
        $price = $product->getPrice();

         # always 1 for a product entity
        $entity_type_id = 1;
        
		$sql = "
		  INSERT INTO product_entity (sku, name, price, category_id, entity_type_id)
		   VALUES('$sku', '$name', '$price', '$category_id','$entity_type_id')";

		if($this->con->query($sql) === TRUE){

		     return true;
		}
		else{
			
			return false;
		}
		$this->con->close();
       
	}
	public function getProductId($sku){

		$empty_data = 0;
		$this->con = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
       
		if($this->con->connect_error){
			die("error".$this->con->connect_error);
		}
		$sql = "SELECT product_id FROM product_entity WHERE sku='$sku'";
		$result = $this->con->query($sql);

		if($result->num_rows > 0){

			$data = $result->fetch_assoc();
			return $data;
		}
		else{
			return $empty_data;
		}
		$this->con->close();

	}
	
	
	public function addAttribute($product_id, $at_name, $at_table, $at_value){


		$this->con = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
       
		if($this->con->connect_error){
			die("error".$this->con->connect_error);
		}
        
      
		$attributes = $this->getAttribute($at_name);

		$attribute_id = $attributes['attribute_id'];
		$sql = "INSERT INTO ".$at_table."(attribute_id, entity_id, value)VALUES('$attribute_id','$product_id','$at_value')";

		$result = $this->con->query($sql);
		if($result === TRUE){
           
            return true;
		}
		else{

       	echo "error ".$sql."".$this->con->error;
		}
		$this->con->close();
	}
	
	public function getAttribute($attribute_code){
      

       $empty_data = [];
      $this->con = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
       
		if($this->con->connect_error){
			die("error".$this->con->connect_error);
		}
		$sql = "SELECT attribute_id FROM aev_attribute WHERE attribute_code = '$attribute_code'";
		$result = $this->con->query($sql);
		if($result->num_rows > 0){
            
            $data = $result->fetch_assoc();
			return $data;
		}
		else{
			return $empty_data;
		}
		$this->con->close();
	}
	public function delete($id){

	    $this->con = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
	    if($this->con->connect_error){
	    	die("connection error:".$this->con->connect_error);
	    }

	    $delete_prod   = "DELETE FROM product_entity WHERE product_id = '$id'";

	    $delete_height = "DELETE FROM product_furniture_height WHERE entity_id = '$id'";
	    $delete_length = "DELETE FROM product_furniture_length WHERE entity_id = '$id'";
	    $delete_width  = "DELETE FROM product_furniture_width WHERE entity_id = '$id'";

	    $delete_weight = "DELETE FROM product_book_weight WHERE entity_id = '$id'";
	    $delete_size   = "DELETE FROM product_dvd_size WHERE entity_id = '$id'";

	    $select_length = "SELECT * FROM product_furniture_length WHERE entity_id = '$id'";
	    $select_width  = "SELECT * FROM product_furniture_width WHERE entity_id = '$id'";
	    $select_height = "SELECT * FROM product_furniture_height WHERE entity_id = '$id'";

	
	    $select_weight = "SELECT * FROM product_book_weight WHERE entity_id = '$id'";
	    $select_size   = "SELECT * FROM product_dvd_size WHERE entity_id = '$id'";

	    $is_delete = false;
	    if($this->con->query($delete_prod))
	    {
	     
	         # if it is a book 
	         $book  = $this->con->query($select_weight);
	         if($book->num_rows > 0){

	         	if($this->con->query($delete_weight)){
	                  # successfully deleted
	         		$is_delete = true;	
	         	}
	         }
	         else
	         {

	         	$dvd = $this->con->query($select_size);
	         	if($dvd->num_rows > 0){

	         		if($this->con->query($delete_size)){
	         			# the product was a dvd and successfully deleted
	         			$is_delete = true;
	         		}
	         	}
	         	else{

	         		$height = $this->con->query($select_height);
	         		$width  = $this->con->query($select_width);
	         		$length = $this->con->query($select_length);
      		      if(($height->num_rows > 0) && ($width->num_rows > 0)){

                  if(($this->con->query($delete_height)) && ($this->con->query($delete_width))
                   && ($this->con->query($delete_length))){
               	    # delete furniture succces
               	    $is_delete = true;
                  }
                  else{
   		      		echo "error ".$height."".$this->con->error;
   		      		echo "error ".$width."".$this->con->error;
   		      		echo "error ".$length."".$this->con->error;
   		         }
      		      
      	      }
      	   }
           }
	      }
	    
	    $this->con->close();
	    return $is_delete;
	}
	public function allProduct(){

   
		$this->con = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
		// Check connection
		if ($this->con->connect_error) {
		  die("connection error :".$this->con->connect_error);
		}
		
		$sql = "
		SELECT e.product_id,e.sku,e.name,e.price,
				at_weight.value as weight, 
				at_size.value as size,
				at_height.value as height,
				at_width.value as width, 
				at_length.value as length

				FROM product_entity as e

				LEFT JOIN product_dvd_size as at_size
				    ON at_size.entity_id = e.product_id 

				LEFT JOIN product_furniture_height as at_height
				    ON at_height.entity_id = e.product_id
				    
				LEFT JOIN product_furniture_width as at_width
				    ON at_width.entity_id = e.product_id
				    
				LEFT JOIN product_furniture_length as at_length
				    ON at_length.entity_id = e.product_id
				    
				LEFT JOIN product_book_weight as at_weight
				    ON at_weight.entity_id = e.product_id

				ORDER BY(e.product_id)
		";
		$data = $this->con->query($sql);
		
		$this->con->close();

		return $data;

	}
	
	public function getCategories(){

		$this->con  = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
		if($this->con->connect_error){
			die("connection error".$this->con->connect_error);
		}
		$sql = "SELECT * FROM product_category_entity";
		$data = $this->con->query($sql);
		
		$this->con->close();
		return $data;
		

	}
	public function getCategoryById($categoryId){

        $data = [];
        if($categoryId == '0'){
           
           return $data;
        }
		$this->con = new mysqli($this->servername, $this->username, $this->password, $this->dbname);

		if($this->con->connect_error){
			die('coonection error'.$this->con->connect_error);
		}
		$sql = "SELECT * FROM product_category_entity WHERE category_id = ".$categoryId;
		$result = $this->con->query($sql);
		if($result->num_rows > 0){
            
			return $result->fetch_assoc();
		}
		else{
               return $data;
		}
		$this->close();

	}
}
