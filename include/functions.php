<?php
 /*
  this function returns true is the form is valid 
  else false
 */
 function is_valid_form($fields = []){
      
      # check wether the array is empty
      if(count($fields) != 0){

        # fields array is not empty
        foreach ($fields as $field) {
            # checking emptyness for each field
            // code...
            if(empty($_POST[$field]) || trim($_POST[$field] == "")){
                # at lease one field has not been filled
                return false;
            }
        }
        # all fields filled
        return true;
      }
 }
 function get_category($categories=[]){

     if(count($categories) == 0){

        echo "not category found";
     }
     while($row = $categories){
        
        return $row;
     }
 }
 function is_valid_furniture_form($fields=[]){

   if(count($fields) != 0){

    foreach($fields as $field){
        if(empty($_POST[$field]) || $_POST[$field] == 0 || $_POST[$field] < 0){
            return false;
        }
    }
    return true;
   }

 }
 

?>