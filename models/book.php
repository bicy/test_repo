<?php

include "models/product.php";


 class Book extends Product{

     private $weight;

     public function getName():string{
      return $this->name;
     }

     public function getSku():string{
      return $this->sku;
     }
     public function getPrice(){
      return $this->price;
     }

     public function setSku($sku){
      $this->sku = $sku;
     }
     public function setName($name){
      $this->name = $name;
     }
     public function setPrice($price){
      $this->price = $price;
     }

     public function getWeight(){

     	return $this->weight;
     }
     public function setWeight($weight){
        $this->weight = $weight;
     }
}
?>