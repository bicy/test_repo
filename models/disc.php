<?php

class Dvd extends Product{

    # specific atribute of Disc Object 
	private $size;

	public function getName():string{
      return $this->name;
    }

    public function getSku():string{
      return $this->sku;
    }
     public function getPrice(){
      return $this->price;
    }

    public function setSku($sku){
      $this->sku = $sku;
    }
    public function setName($name){
      $this->name = $name;
    }
    public function setPrice($price){
      $this->price = $price;
    }
	
	# getter
	public function getSize(){
		return $this->size;
	}
    
	# setter
	public function setSize($size){
		$this->size = $size;
	}
}
?>