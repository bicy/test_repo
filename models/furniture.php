<?php

class Furniture extends Product{

	private $height;
	private $width;
	private $length;

    public function getName():string{
      return $this->name;
     }

     public function getSku():string{
      return $this->sku;
     }
     public function getPrice(){
      return $this->price;
     }

     public function setSku($sku){
      $this->sku = $sku;
     }
     public function setName($name){
      $this->name = $name;
     }
     public function setPrice($price){
      $this->price = $price;
     }

    # getter methods
    public function getHeight(){
    	return $this->height;
    }
    public function getWidth(){
    	return $this->width;
    }
    public function getLength(){
    	return $this->length;
    }

    # setter methods
    public function setHeight($height){
    	$this->height = $height;
    }
    public function setWidth($width){
    	$this->width = $width;
    }
    public function setLength($length){
    	$this->length = $length;
    }
   

}
?>