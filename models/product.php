<?php
abstract class Product{


    protected $sku;
    protected $name;
    protected $price;
     
	abstract public function getName():string;

    abstract public function getSku():string;

    abstract public function getPrice();

    abstract public function setSku($sku);

    abstract public function setName($name);

    abstract public function setPrice($price);
}
?>