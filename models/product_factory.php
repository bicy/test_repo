<?php
include 'book.php';
include 'furniture.php';
include 'disc.php';

abstract class ProductCreator{

	abstract public function factoryMethod($product_type);

}

class ConreteProductCreator extends ProductCreator{

    
   private $product;

   public function factoryMethod($product_type){

   	    if($product_type == "book" || $product_type == 'BOOK' || $product_type == 'Book'){

   	    	$this->product =  new Book();
   	    }
   	    elseif($product_type == 'furniture' || $product_type == 'FURNITURE' || $product_type == 'Furniture'){

   	    	$this->product = new Furniture();
   	    }
   	    elseif($product_type == 'dvd' || $product_type == 'DVD'){

   	    	$this->product = new Dvd();
   	    }
   	   

   	    return $this->product;
   }


}

function factoryClientCode(ProductCreator $creator, string $product_type){

	return $creator->factoryMethod($product_type);
}



?>