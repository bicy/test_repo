<?php
    include 'models/product_factory.php';
    include 'database.php';
    include 'include/functions.php';

    if(isset($_POST['save'])){
        
        
        # extrat post variables to work them as php variable using dollar signe ex: $pr_name, $pr_sku
        extract($_POST);
        # fetching categories from data base using get_category and getCategoryById
        $category = get_category($con->getCategoryById($pr_category));

        # assign category_code to $category_name
        $category_name = $category['category_code'];
        
        # create a new product object using factory design pattern 
        $product_obj = factoryClientCode(new ConreteProductCreator(),$category_name);
        $product_obj->setSku($sku);
        $product_obj->setName($name);
        $product_obj->setPrice($price);

        $category_id = $pr_category;
        $sku = $product_obj->getSku();

        # query to insert new product
        $con->add($product_obj, $category_id);
        
        $product = $con->getProductId($sku);
        $product_id = $product['product_id'];

        if(!empty($_POST['weight']) && $weight > 0){
             
            $product_obj->setWeight($weight);
            $weigth = $product_obj->getWeight();
            $con->addAttribute($product_id,'weight','product_book_weight',$weight);

        }
        elseif(is_valid_furniture_form(['height','width','length'])){

            # when user selects furniture
            $product_obj->setHeight($height);
            $product_obj->setWidth($width);
            $product_obj->setLength($length);
            
            $height = $product_obj->getHeight();
            $width = $product_obj->getWidth();
            $length = $product_obj->getLength();
            # query to insert furniture attributes

            $height_saved = $con->addAttribute($product_id, 'height','product_furniture_height', $height);
            $width_saved  = $con->addAttribute($product_id, 'width','product_furniture_width',$width);
            $length_saved = $con->addAttribute($product_id, 'length','product_furniture_length',$length);

        }
        elseif(!empty($_POST['size'] && $size > 0)){

             # when user selects dvd
            $product_obj->setSize($size);
            $size = $product_obj->getSize();
            $con->addAttribute($product_id,'size','product_dvd_size',$size);
           
        }
   
    }

header("Location:index.php");
