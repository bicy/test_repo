<?php $title = "add Product"; ?>
<!doctype html>
  <style>
    
    .error{
      color: red;
      left: 15px;
    }
    .product_description{
      margin-top: 8px;
    }
  </style>
  <html>
    <?php include("./partials/_header.php")?>
    <body>
       <?php include("./partials/_nav.php");?>
      <div class="container-fluid mt-4">
        <form method='post' action="./save.php" id="product_form" class=''>
           <div class='container-fluid pb-1  mb-3 row' style='border-bottom: 1px solid gainsboro;'>
              <div class='col-lg-3'>
                 <h4>Product Add</h4>
              </div>
              <div class='d-flex justify-content-end col-lg-9'>
                 <input type='submit' class='btn btn-primary ms-2 btn-sm' name='save' value='Save' style='margin-right:8px;'>
                 <a href='./index.php' class='btn btn-secondary btn-sm'>Cancel</a>
              </div>
           </div>
          <div class='col-6'>
            <div class='form-group '>
               <label class='col-form-label'>Sku:</label>
               <input type='input' placeholder="Product Sku" name='sku' class='form-control ' id="sku" required>
               <small class="invalid-feedback"></small>
           
            </div>
            <div class='form-group'>
              <label class='col-form-label'>Name:</label>
              <input type='input' placeholder="Product Name" name='name' class='form-control ' id="name" required>
              <small class="invalid-feedback"></small>
             
           </div>
           <div class='group-form'>
              <label class='col-form-label'>Price:</label>
              <input type='number' placeholder="" min="1" name='price' class='form-control ' id="price" required>
              <small class="invalid-feedback"></small>
             
           </div>
           <div class='group-from'>
            <label class='col-form-label'>Switcher: </label>
             <select class="form-select " aria-label="Default select example" name='pr_category' id='productType' required>
              <option value='0'>Open this select menu</option>
              <?php
                if($data->num_rows > 0){

                     while($row = $data->fetch_assoc()){

                      echo "<option value='".$row['category_id']."'>".$row['category_code']."</option>";
                    }

                }
               
              ?>
            </select>
            <small class="invalid-feedback">Switcher required</small>
            <small class="valid-feedback">valid value</small>
          </div>
          <div id='DVD' class='hidden'>
            <div class='group-form'>
              <label class='col-form-label'>Size(MB): </label>
              <input class='form-control' type="number" id='size' name='size'>
              <small class="text-muted"></small>
            </div>
            <p class='product_description'>Please provide Size</p>
          </div>
            <div id='Book' class='hidden'>
              <div class='group-form'>
                <label class='col-form-label'>Weight(KG): </label>
                <input type="number" name="weight" class='form-control' id="weight">
                <small class="text-muted"></small>
              </div>
              <p class='product_description'>Please provide Weight</p>
            </div>
            <div id='Furniture' class='hidden'>
                <div class='form-group'>
                  <label>Height(CM): </label>
                  <input type='number' class='form-control' name='height' id='height'>
                  <small class="text-muted"></small>
                
                </div>
                <div class='form-group'>
                   <label>Width(CM): </label>
                   <input type='number' class='form-control' name='width' id='width'>
                   <small class="text-muted"></small>
                  
                </div>
                <div class='form-group'>
                  <label>Length(CM): </label>
                  <input type='number' class='form-control' name='length' id='length'>
                  <small class="text-muted"></small>
                </div>
                <p class='product_description'>Please provide dimension</p>
            </div>
          </div>
        </form>
      </div>
     <?php include("partials/_footer.php")?>
    </body>
  </html>