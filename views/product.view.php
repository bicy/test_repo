<?php
 include "./include/functions.php";
 $title = "Product List";
?>
<!doctype html>
  <html>
    <?php include("./partials/_header.php")?>
    <body>
      <?php include("./partials/_nav.php");?>
      <div class="container-fluid mt-4">
          <form action='./delete.php' method='post'>
             <div class='container-fluid pb-1  mb-3 row' style='border-bottom: 1px solid gainsboro;'>
              <div class='col-lg-3'>
                 <h4>Product List</h4>
              </div>
              <div class='d-flex justify-content-end col-lg-9'>
                 <input type="submit" name='add' class="btn btn-dark btn-sm" value='ADD' id='ID' style='margin-right:8px;'>
                 <input type="submit" name='delete' class="btn btn-danger btn-sm" value='MASS DELETE'>
              </div>
          </div>
           
              <?php
                if($data->num_rows == 0){

                    echo "<h2>Opps ! sorry no product to display, please add some</h2>";
                }

                echo " <div class='row row-cols-1 row-cols-md-4 g-4 mt-2'>";

                while($row = $data->fetch_assoc()){
                    
                    $begin = "<div class='col'>
                                <div class='card bg-light' style='width: 18rem;height:11rem'>
                                    <input type='checkbox' name='checkbox[]' class='ms-2 mt-2 delete-checkbox' id='cardCheck' value='".$row['product_id']."'/>
                                    <div class='card-body ms-5'>
                                        <h6>".$row['sku']."</h6>
                                        <h6>". $row['name']."</h6>
                                        <h6>".$row['price']." $</h6>";
                    $end = " </div>
                                </div>
                            </div>";

                    if($row['weight'] > 0){

                       echo  $begin ."<h6>".$row['weight']."KG</h6>".$end;
                    }
                    else{

                        if($row['height'] > 0|| $row['width'] > 0 || $row['length'] > 0){

                            echo $begin."<h6>".$row['height']."x".$row['width']."x".$row['length']."</h6>".$end;

                        }else{

                            echo $begin."<h6>".$row['size']." MB</h6>".$end;
                        }
                    }                     
                } 
                echo "</div>";          
              ?>
        </form>
      </div>
      <?php include("partials/_footer.php")?>
    </body>
  </html>

 